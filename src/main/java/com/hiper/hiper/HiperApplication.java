package com.hiper.hiper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HiperApplication {

	public static void main(String[] args) {
		SpringApplication.run(HiperApplication.class, args);
	}

}
